#!/bin/bash
set -euo pipefail

function usage {
    echo "usage: download.sh <parameter_profile_file> <target_dir>"
    exit 1
}

if [ $# -ne 2 ]; then
    usage
fi
PARAMETER_PROFILE_FILE=$1
TARGET_DIR=$2

if [ ! -f $PARAMETER_PROFILE_FILE ]; then
    echo "Parameter profile file not found: $PARAMETER_PROFILE_FILE"
    usage
fi


if [ ! -d $TARGET_DIR ]; then
    echo "Target dir not found: $TARGET_DIR"
    usage
fi

echo "download data to target dir: $TARGET_DIR"

cd $TARGET_DIR
# Your code here

# 0) download Data.gouv + INSEE data (contours + commmunes pop)

CONTOURS_URL='https://www.data.gouv.fr/fr/datasets/r/07b7c9a2-d1e2-4da6-9f20-01a7b72d4b12'
ZIP_FILE_NAME="communes-20190101.zip"
CONTOURS_FILE_NAME="communes-20190101.json"

wget -q $CONTOURS_URL -O $ZIP_FILE_NAME
unzip -p $ZIP_FILE_NAME $CONTOURS_FILE_NAME > $CONTOURS_FILE_NAME
echo "downloaded Communes contours"

COMMUNE_POP_URL="https://www.insee.fr/fr/statistiques/fichier/4989724/ensemble.zip"
ZIP_FILE_NAME="ensemble.zip"
COMMUNE_FILE_NAME="Communes.csv"
wget -q $COMMUNE_POP_URL -O $ZIP_FILE_NAME
unzip -p $ZIP_FILE_NAME $COMMUNE_FILE_NAME > $COMMUNE_FILE_NAME

echo "downloaded Communes population"

# 1) download blocks (cycles) pop density

CYCLES_TERRITORY=`jq -r '.cycles_territory' $PARAMETER_PROFILE_FILE`
echo "CYCLES TERRITORY: $CYCLES_TERRITORY"

REQ="https://api.openmobilityindicators.org/indicator-data?parameter_profile="$CYCLES_TERRITORY"&slug=population-density-from-cycles&sort=created_at%3Adesc&page=1&size=1"

wget -q "$REQ" -O response
RAWID=`jq -r '.indicator_data| .items| .[0]| .id' response`
echo "RAWID: $RAWID"

POPDENSITY_URL="https://files.openmobilityindicators.org/indicator-data/"$RAWID"/pop_density.geojson"
echo "POPDENSITY_URL: $POPDENSITY_URL"

echo "download ped network blocks geojson file to target dir: $TARGET_DIR"
wget -q $POPDENSITY_URL -O pop_density.geojson
echo "done."

# 2) download way types

WAYTYPES_TERRITORY=`jq -r '.waytypes_territory' $PARAMETER_PROFILE_FILE`
echo "CYCLES TERRITORY: $WAYTYPES_TERRITORY"

REQ="https://api.openmobilityindicators.org/indicator-data?parameter_profile="$WAYTYPES_TERRITORY"&slug=pedestrian-way-types&sort=created_at%3Adesc&page=1&size=1"

wget -q "$REQ" -O response
RAWID=`jq -r '.indicator_data| .items| .[0]| .id' response`
echo "RAWID: $RAWID"

WAYTYPES_URL="https://files.openmobilityindicators.org/indicator-data/"$RAWID"/way_types.geojson"
echo "WAYTYPES_URL: $WAYTYPES_URL"

echo "download way types geojson file to target dir: $TARGET_DIR"
wget -q $WAYTYPES_URL -O way_types.geojson
echo "done."
