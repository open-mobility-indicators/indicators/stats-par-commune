# OMI notebook indicator : "Stats par commune française"
## description : [indicator.yml FILE](https://gitlab.com/open-mobility-indicators/indicators/stats-par-commune/-/blob/main/indicator.yaml)

Stats par commune

An indicator project based on the indicator template Jupyter notebook ready to be used by the pipeline Input : an OMI score GeoJSON file produced Output : a CSV file containing statistics by French commune

Input Data:
- geojson :  https://files.openmobilityindicators.org/indicators-output/compute-notebooks-run-ndcnm/omi_score/omi_score.geojson ilots PACA
- commune contours json 2019 OSM : https://www.data.gouv.fr/fr/datasets/r/07b7c9a2-d1e2-4da6-9f20-01a7b72d4b12
- population légale 2018 : https://www.insee.fr/fr/statistiques/fichier/4989724/ensemble.zip

forked from [jupyter notebook template](https://gitlab.com/open-mobility-indicators/indicator-templates/jupyter-notebook)

[Install locally using a venv or Docker, Build and Use (download, compute)](https://gitlab.com/open-mobility-indicators/website/-/wikis/2_contributeur_technique/install-a-notebook-locally)